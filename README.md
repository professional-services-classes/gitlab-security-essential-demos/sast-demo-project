### SAST, Code Quality, and Secret

The branch `glen_miller-master-patch-69449` has the full SAST, CQ, and Secret setup. Please note secrets only detected items that Gitleaks can find. See the Security and Code Quality tabs on the pipeline for more.

### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.

1. Just testing some indents
    - Does this look right?
2. Some more
    - Indent it again
